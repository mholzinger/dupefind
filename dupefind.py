import os
import zlib
import sys
import argparse
from concurrent.futures import ThreadPoolExecutor, as_completed

# Helper function to convert bytes to a more readable format
def bytes_to_readable(size_in_bytes):
    for unit in ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB']:
        if size_in_bytes < 1024.0:
            return f"{size_in_bytes:3.1f} {unit}"
        size_in_bytes /= 1024.0
    return f"{size_in_bytes:.1f} YB"

# Function to compute CRC32 of a file and get its size
def file_hash_crc32_and_size(file_path):
    crc32 = 0
    file_size = 0
    try:
        with open(file_path, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b''):
                crc32 = zlib.crc32(chunk, crc32)
                file_size += len(chunk)
    except Exception as e:
        print(f"\nError reading file {file_path}: {e}")
        return None, file_path, 0
    return f"{crc32 & 0xFFFFFFFF:08x}", file_path, file_size

def format_path(path):
    if len(path) > 64:
        return f"{path[:30]}...{path[-27:]}"
    else:
        return path

def should_skip(directory, skip_paths):
    for skip_path in skip_paths:
        if directory.startswith(skip_path):
            return True
    return False

def scan_directories(directories, skip_paths=None):
    if skip_paths is None:
        skip_paths = []
    file_paths = []
    for directory_path in directories:
        for root, dirs, files in os.walk(directory_path, topdown=True):
            if should_skip(root, skip_paths):
                dirs[:] = []  # Skip this directory and all subdirectories
                continue
            displayed_path = format_path(root)
            sys.stdout.write(f"\rScanning directory: {displayed_path}" + " " * 10)
            sys.stdout.flush()
            for file in files:
                file_paths.append(os.path.join(root, file))
    sys.stdout.write('\r' + ' ' * 100 + '\r')  # Clear the line after scanning is complete
    sys.stdout.flush()
    return file_paths

def find_duplicates(directories, skip_paths=None):
    file_paths = scan_directories(directories, skip_paths)
    total_files = len(file_paths)
    print(f"\nFound {total_files} files across directories, computing hashes...")

    # Compute hashes and collect file sizes in parallel
    hash_dict = {}
    processed_files = 0
    with ThreadPoolExecutor(max_workers=10) as executor:
        future_to_file = {executor.submit(file_hash_crc32_and_size, file_path): file_path for file_path in file_paths}

        for future in as_completed(future_to_file):
            file_hash, file_path, file_size = future.result()
            processed_files += 1
            sys.stdout.write(f"\rProcessed {processed_files}/{total_files} files...")
            sys.stdout.flush()
            if file_hash:
                if file_hash in hash_dict:
                    hash_dict[file_hash].append((file_path, file_size))
                else:
                    hash_dict[file_hash] = [(file_path, file_size)]

    print("\n")  # New line after processing

    # Identify duplicates and print total file size for each group
    duplicates = {hash: files for hash, files in hash_dict.items() if len(files) > 1}
    if duplicates:
        print("Duplicate files found across directories:")
        for hash, files in duplicates.items():
            total_size = sum(file[1] for file in files)
            readable_size = bytes_to_readable(total_size)
            print(f"\nDuplicates (CRC32: {hash[:6]}) - Total Size: {readable_size}:")
            for file in files:
                print(file[0])
    else:
        print("No duplicate files found across directories.")

def expand_and_split_paths(raw_paths):
    """Expand user tilde and split by commas, if necessary."""
    expanded_paths = []
    for path in raw_paths:
        # Split paths by commas, expand each path, and strip quotes/spaces
        for sub_path in path.split(','):
            expanded_path = os.path.expanduser(sub_path.strip(' "\''))
            expanded_paths.append(expanded_path)
    return expanded_paths

def main(input_paths, skip_paths=None, sort_by_size=False):
    file_paths = scan_directories(input_paths, skip_paths)
    total_files = len(file_paths)
    print(f"\nFound {total_files} files across directories, computing hashes...")

    # Compute hashes and collect file sizes in parallel
    hash_dict = {}
    with ThreadPoolExecutor(max_workers=10) as executor:
        future_to_file = {executor.submit(file_hash_crc32_and_size, file_path): file_path for file_path in file_paths}

        for future in as_completed(future_to_file):
            file_hash, file_path, file_size = future.result()
            if file_hash and file_size > 0:  # Ensure file size is greater than zero
                if file_hash in hash_dict:
                    hash_dict[file_hash].append((file_path, file_size))
                else:
                    hash_dict[file_hash] = [(file_path, file_size)]

    print("\n")  # New line after processing

    # Sort duplicates by total size if requested and ensure no zero-byte files
    if sort_by_size:
        duplicates = sorted(
            ((hash, [file for file in files if file[1] > 0]) for hash, files in hash_dict.items() if len(files) > 1),
            key=lambda item: sum(file[1] for file in item[1]), reverse=True)
    else:
        duplicates = [(hash, [file for file in files if file[1] > 0]) for hash, files in hash_dict.items() if len(files) > 1]

    if duplicates:
        print("Duplicate files found across directories:")
        for hash, files in duplicates:
            if not files:  # Skip this group if all files were zero-byte
                continue
            total_size = sum(file[1] for file in files)
            readable_size = bytes_to_readable(total_size)
            print(f"\nDuplicates (CRC32: {hash[:6]}) - Total Size: {readable_size}:")
            for file in files:
                print(file[0])
    else:
        print("No duplicate files found across directories.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Find duplicate files across provided directories and their total size.")
    parser.add_argument('--input', nargs='+', help='Directories to scan', required=True)
    parser.add_argument('--skip', nargs='*', default=[], help='Directories to skip')
    parser.add_argument('--sort', action='store_true', help='Sort duplicate file sets by total size, descending')
    args = parser.parse_args()

    # Ensure paths are expanded and properly handled
    input_paths = expand_and_split_paths(args.input)
    skip_paths = expand_and_split_paths(args.skip) if args.skip else None

    main(input_paths, skip_paths, args.sort)

